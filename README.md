## Takes this
![image](https://gitlab.com/dudethatbe/traktor-playlist-webpage-to-csv/-/wikis/uploads/97c5480c4b25f93344397dd129b20909/image.png)

## And turns it into this
![image](https://gitlab.com/dudethatbe/traktor-playlist-webpage-to-csv/-/wikis/uploads/6d39ba74c217f53a927f3cf6cef1d236/image.png)


### usage

Traktor has a "Save as webpage" right-click menu for playlists. This is helpful for the "History" section to keep records of which tracks you played in a set.
The HTML is fine, but isn't flexible. This will read the webpage generated from Traktor and write it out to a plain CSV file with less fields

Note: Do not change the default fields when saving the webpage >:)

### how to

```
$ npm install
```

Save html file as `input.html` and save to project root

```
$ npm start
```

This will create a `output.csv` file
