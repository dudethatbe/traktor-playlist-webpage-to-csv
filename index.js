import { readFile, writeFile } from "fs/promises";
import { parse } from "parse5";
import { Parser } from "json2csv";
import path from "path/win32";
// Default fields from Traktor when saving webpage
const Columns = {
  Num: "Num.",
  Color: "Color",
  Title: "Title",
  Artist: "Artist",
  Time: "Time",
  BPM: "BPM",
  Track: "Track",
  Release: "Release",
  Label: "Label",
  Genre: "Genre",
  Key_Text: "Key Text",
  Key: "Key",
  Comment: "Comment",
  Lyrics: "Lyrics",
  Rating: "Rating",
  File_Path: "File Path",
  File_Name: "File Name",
  Analyzed: "Analyzed (Peak, Perceived)",
  Remixer: "Remixer",
  Producer: "Producer",
  Release_Date: "Release Date",
  Bitrate: "Bitrate",
  Comment2: "Comment2",
  Play_Count: "Play Count",
  Last_Played: "Last Played",
  Import_Date: "Import Date",
  Start_Time: "Start Time",
  Duration: "Duration",
  Deck: "Deck",
  Played_Public: "Played Public",
};
// Preferred fields to be displayed. Add/Remove fields from default set IN ORDER :)
const MyColumns = {
  Num: "Num.",
  Title: "Title",
  Key: "Key",
  BPM: "BPM",
  Bitrate: "Bitrate",
  Time: "Time",
  Duration: "Duration",
  File_Path: "File Path",
  Import_Date: "Import Date",
  Play_Count: "Play Count",
  Start_Time: "Start Time",
  Deck: "Deck",
  Played_Public: "Played Public",
};

// Used for mapping the *sometimes strange field names to an easy one
function getKeyByValue(object, value) {
  return Object.keys(object).find((key) => object[key] === value);
}

// HTML selection helper (html > body > table > ...)
function getFromChildNodes(element, tagName) {
  return element.childNodes.find((childNode) => {
    return childNode.tagName === tagName;
  });
}

async function parsePlaylist(input) {
  const contents = await readFile(input, "utf-8");
  const parsed = parse(contents);

  const html = getFromChildNodes(parsed, "html");
  const body = getFromChildNodes(html, "body");
  const table = getFromChildNodes(body, "table");
  const tbody = getFromChildNodes(table, "tbody");

  let mappedKeys = {};
  let tracks = [];

  for (const row of tbody.childNodes) {
    let playlistTrack = {};

    // table has alternating tr and #text childNodes
    if (row.tagName === "tr") {
      let headerIndex = 0;
      for (const cell of row.childNodes) {
        if (cell.tagName === "th") {
          const headerText = cell.childNodes.find((x) => {
            return x.nodeName === "#text";
          });

          // make a mappedKeys = { 1 = "Number", ... }
          const key = getKeyByValue(Columns, headerText.value);
          mappedKeys[headerIndex] = key;
        }
        if (cell.tagName === "td") {
          const cellText = cell.childNodes.find((x) => {
            return x.nodeName === "#text";
          });
          if (cellText) {
            // add to track.Number = "10";
            playlistTrack[mappedKeys[headerIndex]] = cellText.value
              .replace(/\n/g, "")
              .replace(/\s{2,}/g, " ");
          }
        }
        headerIndex++;
      }
      if (headerIndex > Object.keys(Columns).length) {
        tracks.push(playlistTrack);
      }
    }
  }

  // remove columns from tracks that aren't preferred
  const myKeys = Object.keys(MyColumns);
  for (let track of tracks) {
    for (const trackKey of Object.keys(track)) {
      if (myKeys.indexOf(trackKey) == -1) {
        delete track[trackKey];
      }
    }
  }

  // delete tracks that weren't actually played and remove filenames from path
  const replaceString = "C:\\Users\\dc\\Dropbox\\dj";
  const playedTracks = tracks
    .filter((track) => track.Played_Public === "yes")
    .map((track) => {
      let temp = track;
      temp.File_Path = path
        .parse(track.File_Path)
        .dir.replace(replaceString, "");
      delete temp.Played_Public;
      return temp;
    });

  const parser = new Parser();
  const csv = parser.parse(playedTracks);
  await writeFile("./output.csv", csv);
}

parsePlaylist("./input.html");
